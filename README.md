object2mqtt

Camera person detection using Yolov3 to mqtt event.
    
    sudo git clone https://ri100@bitbucket.org/ri100/python.object2mqtt.git /opt/object2mqtt
    sudo -H pip3 install -r /opt/object2mqtt/requirements.txt
    sudo chmod 755 /opt/object2mqtt/object2mqtt
    sudo cp /opt/object2mqtt/install/etc/object2mqtt.conf /etc/object2mqtt.conf
    sudo cp /opt/object2mqtt/install/systemd/object2mqtt.service /etc/systemd/system/object2mqtt.service
    sudo chown root:root /etc/systemd/system/object2mqtt.service
    sudo chmod 664 /etc/systemd/system/object2mqtt.service

    sudo systemctl daemon-reload