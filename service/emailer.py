import smtplib
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart


def send_email(from_address, recipients,
               subject, body,
               login, password,
               attachment,
               smtp_server='smtp.gmail.com:587'):
    recipients_list = [elem.strip().split(',') for elem in recipients]

    message = MIMEMultipart()
    message['Subject'] = str(subject)
    message['From'] = from_address
    message['Reply-to'] = from_address

    message.preamble = 'Multipart massage.\n'

    part = MIMEText(body)
    message.attach(part)

    part = MIMEApplication(open(str(attachment), "rb").read())
    part.add_header('Content-Disposition', 'attachment', filename=str(attachment))
    message.attach(part)

    server = smtplib.SMTP(smtp_server)
    server.ehlo()
    server.starttls()
    server.login(login, password)
    problems = server.sendmail(from_address, recipients_list, message.as_string())
    server.quit()
    return problems


if __name__ == "__main__":
    send_email(from_address='ramtamtam73@gmail.com',
               recipients=['s2effy4jbj@pomail.net'],
               subject="Intruder detected at 2018-02-18",
               body="Monitor 1", login='ramtamtam73@gmail.com',
               password="strych12345", attachment='/home/risto/Pictures/flat1.jpg')
