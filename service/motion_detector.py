import logging
import debug.preview
import cv2
import time
import imutils

from domain.box import Box

logger = logging.getLogger(__name__)


class MotionConfig:

    settings = [
        'suppress', 'suppress_mail', 'expand_area_by', 'resize', 'min_area', 'threshold',
        'min_object_detection_accuracy', 'suppress_object_detection'
    ]

    suppress_mail = False
    min_object_detection_accuracy = .5
    suppress_object_detection = False,
    motion_slow_down = None
    preview = False
    suppress = False
    resize = 350
    expand_area_by = 30
    min_area = 200
    threshold = 15
    pre_process = None
    auto_brightness = False
    roi = None


class MotionDetector:

    def __init__(self, config: MotionConfig, on_motion):
        self.on_motion = on_motion
        self.config = config
        self.last_frame = None
        self.motion_timer = time.time()

    def stream(self, source):
        while True:
            while not self.config.suppress:
                logger.info('Video stream opening.')
                video = cv2.VideoCapture(source)
                logger.info('Video {} stream opened.'.format(source))
                while not self.config.suppress:
                    success, image = video.read()
                    if not success:
                        logger.error('Can not read video.')
                        time.sleep(1 / 12)
                        continue
                    yield image
                if video.isOpened():
                    logger.info('Video stream closed.')
                    video.release()
            time.sleep(5)

    def frames(self, source):
        for i, frame in enumerate(self.stream(source)):

            if frame is None:
                continue

            start = time.time()
            if self.config.pre_process is not None:
                frame = self.config.pre_process(frame)

            full_height, full_width, _ = frame.shape

            self.config.resize = self.config.resize if self.config.resize > 0 else 350

            resized_frame = imutils.resize(frame, height=self.config.resize)

            if self.config.auto_brightness:
                alpha = 1.5  # Contrast control (1.0-3.0)
                beta = 10  # Brightness control (0-100)
                resized_frame = cv2.convertScaleAbs(resized_frame, alpha=alpha, beta=beta)

            resized_height, resized_width, _ = resized_frame.shape
            resized_frame_size = resized_height * resized_width

            gray_frame = cv2.cvtColor(resized_frame, cv2.COLOR_BGR2GRAY)
            blured_resized_frame = cv2.GaussianBlur(gray_frame, (21, 21), 0)

            # Converting color image to gray_scale image
            if self.last_frame is None:
                self.last_frame = blured_resized_frame
                continue

            last_frame_height, last_frame_width = self.last_frame.shape
            if last_frame_height != resized_height or last_frame_width != resized_width:
                # This may happen when resize change
                self.last_frame = blured_resized_frame.copy()
                continue

            difference_frame = cv2.absdiff(self.last_frame, blured_resized_frame)
            self.last_frame = blured_resized_frame.copy()

            alpha = 1.5  # Contrast control (1.0-3.0)
            beta = 0  # Brightness control (0-100)
            difference_frame = cv2.convertScaleAbs(difference_frame, alpha=alpha, beta=beta)
            threshold_frame = cv2.threshold(difference_frame, self.config.threshold, 255, cv2.THRESH_BINARY)[1]
            threshold_frame = cv2.dilate(threshold_frame, None, iterations=2)

            # Finding contour of moving object
            contours = cv2.findContours(threshold_frame.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
            contours = imutils.grab_contours(contours)

            sum_of_areas = 0
            moved_rectangles = []
            for contour in contours:
                # if the contour is too small, ignore it
                area = cv2.contourArea(contour)
                if area < self.config.min_area:
                    logger.debug("Motion contour {} less then {}.".format(area, self.config.min_area))
                    continue

                (x, y, w, h) = cv2.boundingRect(contour)
                moved_rectangles.append([x, y, x + w, y + h])
                sum_of_areas += w * h

            motion_flag = False
            full_size_expanded_box = Box((0, 0, 0, 0))
            motion_box = Box((0, 0, 0, 0))
            expanded_motion_box = Box((0, 0, 0, 0))
            if moved_rectangles:

                # There is motion
                logger.debug("Number of contours {}".format(len(moved_rectangles)))

                motion_box = Box.join(moved_rectangles)

                # Is it in ROI
                if self.config.roi is None or self.config.roi.intersects(motion_box):
                    expanded_motion_box = motion_box.expand(resized_width, resized_height, expand=self.config.expand_area_by)

                    box_scale = full_height / self.config.resize
                    full_size_expanded_box = expanded_motion_box.scale(box_scale)

                    resized_motion_size = motion_box.size()
                    motion_percentage = resized_motion_size / resized_frame_size
                    logger.debug("Motion percentage {}.".format(motion_percentage))

                    if 0.0005 <= motion_percentage <= 0.85:
                        motion_flag = True
                        self.motion_timer = time.time()
                    else:
                        # ostatni raz kiedy był ruch
                        motion_flag = time.time() - self.motion_timer < 1.5

            if self.config.preview:
                debug.preview.Preview.show_motion(resized_frame.copy(), self.config.roi, motion_box, expanded_motion_box, moved_rectangles, delay=1)

            if motion_flag:

                """
                Must slower motion detection frame rate to be able to annotate/detect object on
                slow computers.
                """
                if self.config.motion_slow_down and not self.config.suppress_object_detection:
                    time.sleep(self.config.motion_slow_down)

                took_time = time.time() - start
                logger.info("Motion detection in frame {0:} took {1:.2f}".format(i, took_time))

                if callable(self.on_motion):
                    self.on_motion(frame, full_size_expanded_box)

                yield frame, full_size_expanded_box, motion_flag
