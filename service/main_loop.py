import json
import os
import logging
import time
from configparser import ConfigParser
import cv2
from service.motion_detector import MotionDetector, MotionConfig
from mqtt.client import MqttClient, MqttSubscriber
from service.object_detector import ObjectDetector
from service.notification_email import NotificationEmail
from service.yolo import YoloDetector

logger = logging.getLogger(__name__)


class MainLoop:

    def __init__(self, url, config: ConfigParser, motion_config: MotionConfig):

        self.url = url
        self.motion_config = motion_config
        host = config['mqtt'].get('host', '127.0.0.1')
        port = int(config['mqtt'].get('port', 1883))
        user = config['mqtt'].get('user', None)
        password = config['mqtt'].get('password', None)
        self.prefix = config['mqtt'].get('prefix', 'ids')

        self.mailer = None
        if 'mail' in config:
            mail_to = config['mail'].get('to', None)
            mail_login = config['mail'].get('login', None)
            mail_password = config['mail'].get('password', None)
            if mail_to is not None and mail_login is not None and mail_password is not None:
                logger.info("Notification emails will be sent to {}.".format(mail_to))
                self.mailer = NotificationEmail(login=mail_login, password=mail_password, to=mail_to)

        topic = "{}/SET".format(self.prefix)

        self.mqtt_client = MqttClient(
            server=host, user=user, password=password, port=port,
            subscribe=MqttSubscriber(topic, callback=self._settings)
        )

        self.object_detector = ObjectDetector(object_detection=YoloDetector(),
                                              motion_detection=MotionDetector(motion_config, self.on_motion),
                                              on_detection=self.on_detection)

        logger.info("Object detector ready.")

    def _settings(self, client, user_data, msg):
        logger.debug("Read payload={}".format(msg.payload))

        payload = json.loads(msg.payload.decode('utf-8'))
        topic = "{}/GET".format(self.prefix)

        with self.object_detector.lock:
            logger.debug("Object detector lock.")
            for key, value in payload.items():
                if key in self.object_detector.motion.config.settings:
                    logger.info("Settings changed {}={}".format(key, value))
                    self.object_detector.motion.config.__setattr__(key, value)
                    logger.debug("Publishing {} {}={}".format(topic, key, value))
                    client.publish(topic, json.dumps({key: value}))

        logger.debug("Leaving subscribe.")

    @staticmethod
    def _draw_motion_box(frame, box, color):
        (x1, y1, x2, y2) = box
        cv2.rectangle(frame, (x1, y1), (x2, y2), color, 2)

    @staticmethod
    def _draw_object_box(frame, boxes):
        if boxes is not None:
            for label, box in boxes.items():
                (x1, y1, x2, y2) = box
                cv2.rectangle(frame, (x1, y1), (x2, y2), (0, 0, 255), 1)

    @staticmethod
    def _draw(frame, motion_box, object_boxes):
        MainLoop._draw_motion_box(frame, motion_box, (255, 255, 255))
        MainLoop._draw_object_box(frame, object_boxes)
        return frame

    def on_motion(self, frame, motion_box):
        now = time.time()
        topic = "{}/MOTION".format(self.prefix)
        payload = {
            "time": now,
            "motion": motion_box.coordinates(),
        }
        payload = json.dumps(payload)
        self.mqtt_client.publish(topic, payload)
        logger.info("MQTT publish {} {}".format(topic, payload))

    def on_detection(self, frame, motion_box, object_boxes, accuracy):
        if object_boxes:
            now = time.time()
            topic = "{}/OBJECTS".format(self.prefix)
            payload = {
                "time": now,
                "intruder": 'person' in object_boxes,
                "objects": object_boxes,
                "motion": motion_box.coordinates(),
                "accuracy": accuracy
            }
            payload = json.dumps(payload)
            self.mqtt_client.publish(topic, payload)
            logger.info("MQTT publish {} {}".format(topic, payload))

        if 'person' in object_boxes and self.mailer is not None and not self.motion_config.suppress_mail:
            frame = self._draw(frame, motion_box.coordinates(), object_boxes)
            self.mailer.mail(frame, object_boxes)

    def run(self):

        try:
            for data in self.object_detector.detect_motion(self.url):
                pass

        except (KeyboardInterrupt, SystemExit):
            logger.info('Stopped.')
            os._exit(1)
