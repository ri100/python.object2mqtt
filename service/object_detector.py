import logging
import threading
import time
import cv2
from domain.box import Box
from service.motion_detector import MotionDetector

logger = logging.getLogger(__name__)


class ObjectDetector:

    def __init__(self, object_detection, motion_detection: MotionDetector, on_detection=None):

        """
        Detects objects in frames.

        :type on_detection: callback
        :type motion_detection: motion_detector.MotionDetector
        :type object_detection: yolo.YoloDetector
        """

        self.on_detection = on_detection
        self.object_detector = object_detection
        self.lock = threading.Lock()
        self.motion = motion_detection
        with self.lock:
            self.frame_data = None
            self.frame_number = -1
            self.boxes = {}
            self.accuracy = {}
            self.detection = False

        thread = threading.Thread(target=self._detection_trigger, name="_motion_trigger")
        thread.daemon = True
        thread.start()

    def _detect_objects(self, frame, box):
        """
        :type frame: object
        :type box: Box
        """
        object_boxes = {}
        object_accuracy = {}

        if box.empty():
            return object_boxes, object_accuracy

        objects = {
            "person": 0,
            "car": 2,
            "motorbike": 3,
            "cat": 15,
            "dog": 16
        }
        x1, y1, x2, y2 = box.coordinates()
        cropped_frame = frame[y1:y2, x1:x2]

        if cropped_frame.any():

            try:
                boxes, confidences, class_ids = self.object_detector.annotate(
                    cropped_frame,
                    min_accuracy=self.motion.config.min_object_detection_accuracy)
                for label, class_id in objects.items():
                    if class_id in class_ids:
                        position = class_ids.index(class_id)
                        p1, p2, w, h = boxes[position]
                        object_boxes[label] = [x1 + p1, y1 + p2,
                                               x1 + p1 + w, y1 + p2 + h]
                        object_accuracy[label] = confidences[position]

            except cv2.error as err:
                print(cropped_frame)
                print(str(err))

        return object_boxes, object_accuracy

    def _detection_trigger(self):
        last_frame_number = -1
        last_suppress_state = False

        while True:

            if last_suppress_state != self.motion.config.suppress_object_detection:

                if self.motion.config.suppress_object_detection:
                    logger.info('Object detection stopped.')
                else:
                    logger.info('Object detection started.')

                last_suppress_state = self.motion.config.suppress_object_detection

            while not self.motion.config.suppress_object_detection:

                time.sleep(1 / 6)

                motion = False
                frame = None
                motion_box = Box((0, 0, 0, 0))

                with self.lock:
                    frame_number = self.frame_number
                    if self.frame_data is not None:
                        frame, motion_box, motion = self.frame_data

                if last_frame_number == frame_number:
                    continue

                last_frame_number = frame_number

                if motion:
                    start = time.time()
                    object_boxes, accuracy = self._detect_objects(frame, motion_box)
                    took_time = time.time()-start
                    logger.info("Object detection took {0:.2f}sec.".format(took_time))

                    if self.on_detection is not None:
                        threading.Thread(target=self.on_detection, args=(frame, motion_box, object_boxes, accuracy)).start()

                    with self.lock:
                        self.boxes = object_boxes
                        self.accuracy = accuracy

            time.sleep(5)

    def detect_motion(self, source):

        """
        Detects objects if there is motion.
        """

        for frame_number, frame_data in enumerate(self.motion.frames(source)):

            frame, motion_box, _ = frame_data

            with self.lock:
                self.frame_data = frame_data
                self.frame_number = frame_number
                object_boxes = self.boxes

            yield frame_number, frame, motion_box, object_boxes

            with self.lock:
                self.boxes = {}
                self.accuracy = {}
