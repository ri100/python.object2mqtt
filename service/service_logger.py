import logging
import os


def set_up_logger(file=None, level=None):
    if level is None:
        level = logging.INFO
    format = '%(asctime)s, %(levelname)s, %(name)s,  %(message)s'

    if file is not None:
        accessible = os.access(os.path.dirname(file), os.W_OK)
    else:
        accessible = False

    if not accessible:
        logging.getLogger(__name__).error("Can not read log file at {}".format(file))

    if file is not None and accessible:
        logging.basicConfig(level=level,
                            format=format,
                            datefmt='%m-%d %H:%M:%S',
                            filename=file,
                            filemode='a')
    else:
        logging.basicConfig(level=level,
                            format=format,
                            datefmt='%m-%d %H:%M:%S')
        logging.getLogger(__name__).warning("Logging to console")

