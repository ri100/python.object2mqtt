import logging
import smtplib
import uuid
import os
from cv2 import cv2
from datetime import datetime
from service.emailer import send_email


logger = logging.getLogger(__name__)


class NotificationEmail:

    def __init__(self, login, password, to):
        self.to = to
        self.password = password
        self.login = login

    def mail(self, frame, object_boxes):
        unique_filename = str(uuid.uuid4())
        unique_path = "/tmp/" + unique_filename + ".jpg"
        cv2.imwrite(unique_path, frame)

        now = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        mail_body = "Alert " + str(object_boxes.keys())
        mail_body += "\nDetection time:" + now
        try:
            send_email(from_address=self.login,
                   recipients=[self.to],
                   subject="Intruder detected",
                   body=mail_body, login=self.login,
                   password=self.password, attachment=unique_path)
            logger.info("Notification sent to {}.".format(self.to))
        except smtplib.SMTPServerDisconnected as e:
            logger.error(str(e))

        os.remove(unique_path)

        return True
