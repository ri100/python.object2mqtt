import cv2
from domain.box import Box
from domain.roi import ROI


class Preview:

    @staticmethod
    def _draw_box(frame, box, color, line=2):
        (x1, y1, x2, y2) = box
        cv2.rectangle(frame, (x1, y1), (x2, y2), color, line)

    @staticmethod
    def _draw_object_box(frame, boxes):
        if boxes is not None:
            for box in boxes:
                Preview._draw_box(frame, box, (0, 255, 255), 1)

    @staticmethod
    def _draw_object_boxes(frame, boxes):
        if boxes is not None:
            for label, box in boxes.items():
                Preview._draw_box(frame, box, (0, 255, 255), 1)

    @staticmethod
    def _show_frame(name, frame, delay):
        cv2.imshow(name, frame)
        if delay is not None:
            cv2.waitKey(delay)

    @staticmethod
    def show_motion(frame, roi: ROI, motion_box: Box, expanded_motion_box: Box, moved_rectangles, delay=None):
        Preview._draw_box(frame, motion_box.coordinates(), (200, 200, 200), 1)
        Preview._draw_box(frame, expanded_motion_box.coordinates(), (0, 0, 255))
        if isinstance(roi, ROI):
            for label, box in roi.boxes.items():
                Preview._draw_box(frame, box.coordinates(), (255, 255, 0))
        if len(moved_rectangles) > 0:
            Preview._draw_object_box(frame, moved_rectangles)
        Preview._show_frame('motion', frame, delay)

    @staticmethod
    def show_objects(frame, motion_box: Box, object_boxes: dict, delay=None):
        Preview._draw_box(frame, motion_box.coordinates(), (200, 200, 200), 1)
        Preview._draw_object_boxes(frame, object_boxes)
        Preview._show_frame('objects', frame, delay)
