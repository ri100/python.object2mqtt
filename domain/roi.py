from typing import Dict

from domain.box import Box


class ROI:

    def __init__(self, boxes: Dict[str, Box]):
        if not isinstance(boxes, dict):
            raise ValueError("ROI is not dict")
        self.boxes = boxes

    def intersects(self, box_in: Box):
        for label, box in self.boxes.items():
            if box.intersects(box_in):
                return True
        return False

    def __repr__(self):
        return "ROI({})".format(self.boxes)


# if __name__ == "__main__":
#     print(ROI([Box((0, 0, 0, 0)), Box((1, 2, 2, 3))]))
