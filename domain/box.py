class Box:

    def __init__(self, coordinates):
        self.x1, self.y1, self.x2, self.y2 = coordinates

    def intersects(self, box):
        """
        Finds if box touches box
        :type box: Box
        """
        vertical_left = left = self.x1 <= box.x1 <= self.x2
        vertical_right = right = self.x1 <= box.x2 <= self.x2
        horizontal_top = top = self.y1 <= box.y1 <= self.y2
        horizontal_bottom = bottom = self.y1 <= box.y2 <= self.y2

        out_vertical_left = box.x1 <= self.x1 <= box.x2
        out_vertical_right = box.x1 <= self.x2 <= box.x2
        out_horizontal_top = box.y1 <= self.y1 <= box.y2
        out_horizontal_bottom = box.y1 <= self.y2 <= box.y2

        corner_inside = left and top or \
                        left and bottom or \
                        right and top or \
                        right and bottom

        if corner_inside:
            return True

        edge_inside = vertical_left and out_vertical_right and out_horizontal_top and out_horizontal_bottom or \
                      vertical_right and out_vertical_left and out_horizontal_top and out_horizontal_bottom or \
                      horizontal_top and out_horizontal_bottom and out_vertical_left and out_vertical_right or \
                      horizontal_bottom and out_horizontal_top and out_vertical_left and out_vertical_right

        if edge_inside:
            return True

        x1_outside = self.x1 >= box.x1
        x2_outside = self.x2 <= box.x2
        y1_outside = self.y1 >= box.y1
        y2_outside = self.y2 <= box.y2

        return x1_outside and x2_outside and y1_outside and y2_outside

    def expand(self, width, height, expand=50):
        """
        Expands box.
        :type expand: int
        :type height: int
        :type width: int
        :return Box
        """
        x1, y1, x2, y2 = self.x1, self.y1, self.x2, self.y2
        if y2 - y1 > 1 and x2 - x1 > 1:
            y2 += expand
            x2 += expand
            x1 -= expand
            y1 -= expand

            x1 = max(0, x1)
            y1 = max(0, y1)
            x2 = min(x2, width)
            y2 = min(y2, height)

        return Box((x1, y1, x2, y2))

    def size(self):
        return (self.x2 - self.x1) * (self.y2 - self.y1)

    def scale(self, scale):
        """
        :type scale: int
        :return Box
        """
        coordinates = int(scale * self.x1), int(scale * self.y1), int(scale * self.x2), int(scale * self.y2)

        return Box(coordinates)

    @staticmethod
    def join(boxes, max_region=None):
        if max_region is None:
            max_region = (0, 0, 1920, 1080)

        max_x2, max_y2, min_x1, min_y1 = max_region

        if not len(boxes):
            max_x2 = 0
            max_y2 = 0
            min_x1 = 0
            min_y1 = 0
        else:
            for (x1, y1, x2, y2) in boxes:
                max_x2 = max(max_x2, x2)
                max_y2 = max(max_y2, y2)
                min_x1 = min(min_x1, x1)
                min_y1 = min(min_y1, y1)

        return Box((min_x1, min_y1, max_x2, max_y2))

    def empty(self):
        return (self.x1, self.y1, self.x2, self.y2) == (0, 0, 0, 0)

    def coordinates(self):
        return self.x1, self.y1, self.x2, self.y2

    def top_coordinates(self):
        return self.x1, self.y1

    def bottom_coordinates(self):
        return self.x2, self.y2

    def __repr__(self):
        return str(self.coordinates())


# if __name__ == "__main__":
#     # b1 = (0, 0, 200, 70)
#     # b2 = (420, 17, 466, 65)
#
#     b1 = (50, 50, 100, 100)
#     b2 = (0, 0, 100, 100)
#
#     b = Box(b1)
#     print(b.intersects(Box(b2)))
#
#     import numpy as np
#     import cv2
#
#     img = np.zeros((500, 500, 3), dtype="uint8")
#     cv2.rectangle(img, (b1[0], b1[1]), (b1[2], b1[3]), (255, 0, 0), 2)
#     cv2.rectangle(img, (b2[0], b2[1]), (b2[2], b2[3]), (0, 255, 0), 2)
#     cv2.imshow("sa", img)
#     cv2.waitKey(100000)
